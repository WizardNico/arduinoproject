# Presentazione: #
* **Esame**: Sistemi Embedded;
* **Data Inizio Lavori**: 13/11/2014;
* **Data Fine Lavori**: 15/12/2014;
* **Componenti Gruppo**: Andrea Nicolini, 652277, andrea.nicolini4@studio.unibo.it;

# Dettagli Tecnici: #
* **Tipologia**: Gestionale Web (Con Servizi RESTful), di un Arduino mediante comunicazione Json;
* **Interfaccia e piattaforma**: C#(Entity Framework 6.0), Web, ASP.NET, SQLServer 2012, Arduino;
* **Titolo**: GuestCounter;