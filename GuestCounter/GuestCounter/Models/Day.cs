﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuestCounter.Models {
	public class Day {
		public Int32? EventId { get; set; }
		public Int32? DayId { get; set; }
		public Int32? NumberOfVisitors { get; set; }
		public Int32? MaximumParticipantsNumber { get; set; }
		public Boolean? Done { get; set; }
		public Boolean? Marked { get; set; }
	}
}