﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuestCounter.Models {
	public class Event {
		public Int32 EventId { get; set; }
		public Int32? TotalParticipants { get; set; }
		public String Title { get; set; }
		public Int32? TiketPrice { get; set; }
		public String Place { get; set; }
		public String Description { get; set; }
	}
}