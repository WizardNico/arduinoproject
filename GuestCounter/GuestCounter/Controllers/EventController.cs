﻿using GuestCounter.Commands.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GuestCounter.Utility;
using GuestCounter.Errors;
using GuestCounter.Models;
using GuestCounter.Commands.Counter;

namespace GuestCounter.Controllers {
	public class EventController : ApiController {
		private dbGuestCounterEntities dbGuestCounter;

		public EventController() { dbGuestCounter = new dbGuestCounterEntities(); }

		[ActionName("addEvent")]
		public Model<Object> POST([FromBody] AddEventCMD cmd) {

			if (cmd.MaximumParticipantsNumber == null || cmd.MaximumParticipantsNumber == 0)
				return (new Model<Object>(EventErrors.InvalidParticipantsNumber));

			string CurrentURL = Request.RequestUri.AbsoluteUri;

			if (cmd.Title == null || cmd.Title.Equals(""))
				return (new Model<Object>(EventErrors.InvalidTitle));

			evento controlTitle = (from e in dbGuestCounter.evento
										  where e.evt_titolo.Trim().ToLower().CompareTo(cmd.Title.Trim().ToLower()) == 0
										  select e).FirstOrDefault();

			if (controlTitle != null)
				return (new Model<Object>(EventErrors.DuplicateEventTitle));

			if (cmd.NumberOfDays == null || cmd.NumberOfDays == 0)
				return (new Model<Object>(EventErrors.InvalidNumberOfDays));

			if (cmd.TiketPrice == null || cmd.TiketPrice < 0)
				cmd.TiketPrice = 0;

			evento evt = new evento();
			evt.evt_luogo = cmd.Place;
			evt.evt_descrizione = cmd.Description;
			evt.evt_prezzoBiglietto = (Int32)cmd.TiketPrice;
			evt.evt_titolo = cmd.Title;

			for (int i = 0; i < cmd.NumberOfDays; i++) {
				giornata grn = new giornata();
				grn.grn_numeroIngressi = 0;
				grn.grn_maxPartecipanti = (Int32)cmd.MaximumParticipantsNumber;
				grn.grn_evt_FK = evt.evt_ID;
				grn.grn_contato = false;
				grn.grn_marcato = false;
				evt.giornata.Add(grn);
			}

			dbGuestCounter.evento.Add(evt);
			dbGuestCounter.SaveChanges();

			return new Model<object>();
		}

		[ActionName("getMarkedDay")]
		public Model<Day> GET([FromUri] GetMarkedDayCMD cmd) {
			giornata grn = (from g in dbGuestCounter.giornata
								 where g.grn_marcato == true
								 select g).FirstOrDefault();

			if (grn == null) {
				return (new Model<Day>(EventErrors.NoMarkedDay));
			} else {
				Day day = new Day();
				day.DayId = grn.grn_ID;
				day.EventId = grn.grn_evt_FK;
				day.Done = grn.grn_contato;
				day.Marked = grn.grn_marcato;
				day.MaximumParticipantsNumber = grn.grn_maxPartecipanti;
				day.NumberOfVisitors = grn.grn_numeroIngressi;
				return (new Model<Day>(day));
			}
		}

		[ActionName("selectDay")]
		public Model<Object> POST([FromBody] SelectDayCMD cmd) {
			giornata grn = (from g in dbGuestCounter.giornata
								 where g.grn_ID == cmd.DayId
								 select g).FirstOrDefault();

			if (grn == null) {
				return (new Model<Object>(CounterErrors.InexistentDay));
			} else {
				foreach (giornata g in dbGuestCounter.giornata.ToList())
					g.grn_marcato = false;

				grn.grn_marcato = true;
				dbGuestCounter.SaveChanges();
				return (new Model<Object>());
			}
		}

		[ActionName("deselectDays")]
		public Model<Object> POST([FromBody] DeselectDaysCMD cmd) {
			foreach (giornata g in dbGuestCounter.giornata.ToList())
				g.grn_marcato = false;

			dbGuestCounter.SaveChanges();
			return (new Model<Object>());
		}

		[ActionName("getDays")]
		public Model<Days> GET([FromUri] GetDaysCMD cmd) {
			Days result = new Days();
			result.DaysList = dbGuestCounter.giornata
				.Where(gio => gio.evento.evt_ID == cmd.EventId)
				.Select(gio => new Day() {
					DayId = gio.grn_ID,
					EventId = gio.grn_evt_FK,
					Marked = gio.grn_marcato,
					MaximumParticipantsNumber = gio.grn_maxPartecipanti,
					NumberOfVisitors = gio.grn_numeroIngressi,
					Done = gio.grn_contato,
				})
				.ToList();
			return (new Model<Days>(result));
		}

		[ActionName("getEvent")]
		public Model<Event> GET([FromUri] GetEventCMD cmd) {
			evento e = null;

			if (cmd.Title != null) {
				e = dbGuestCounter.evento
				.Where(evt => evt.evt_titolo.Trim().ToLower().CompareTo(cmd.Title.Trim().ToLower()) == 0)
				.FirstOrDefault();
			} else {
				if (cmd.EventId != 0 || cmd.EventId != null)
					e = dbGuestCounter.evento.Where(evt => evt.evt_ID == cmd.EventId).FirstOrDefault();
			}

			if (e == null) {
				return (new Model<Event>(EventErrors.InvalidEventID));
			} else {
				Event result = new Event();
				result.Description = e.evt_descrizione;
				result.EventId = e.evt_ID;
				result.Place = e.evt_luogo;
				result.TiketPrice = e.evt_prezzoBiglietto;
				result.Title = e.evt_titolo;
				return (new Model<Event>(result));
			}
		}

		[ActionName("getEvents")]
		public Model<Events> GET([FromUri] GetEventsCMD cmd) {
			Events result = new Events();
			result.EventsList = dbGuestCounter.evento
				.Select(e => new Event() {
					EventId = e.evt_ID,
					Description = e.evt_descrizione,
					Place = e.evt_luogo,
					TiketPrice = e.evt_prezzoBiglietto,
					Title = e.evt_titolo,
				})
				.ToList();
			return (new Model<Events>(result));
		}

		[ActionName("getCompletedEvents")]
		public Model<Events> GET([FromUri] GetCompletedEventsCMD cmd) {
			Events result = new Events();
			result.EventsList = new List<Event>();
			bool add = true;
			int counter = 0;

			foreach (evento e in dbGuestCounter.evento.ToList()) {
				add = true;
				counter = 0;
				foreach (giornata g in e.giornata.ToList()) {
					if (g.grn_contato == false) {
						add = false;
						break;
					} else {
						counter += g.grn_numeroIngressi;
					}
				}
				if (add) {
					Event eventToAdd = new Event();
					eventToAdd.EventId = e.evt_ID;
					eventToAdd.Description = e.evt_descrizione;
					eventToAdd.Place = e.evt_luogo;
					eventToAdd.TiketPrice = e.evt_prezzoBiglietto;
					eventToAdd.Title = e.evt_titolo;
					eventToAdd.TotalParticipants = counter;
					result.EventsList.Add(eventToAdd);
				}
			}

			if (result.EventsList.Count() >= 2)
				return (new Model<Events>(result));
			else
				return (new Model<Events>(EventErrors.NotEnoughEvents));
		}


		protected override void Dispose(bool disposing) {
			base.Dispose(disposing);
			dbGuestCounter.Dispose();
		}
	}
}

