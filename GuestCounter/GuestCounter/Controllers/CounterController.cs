﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GuestCounter.Utility;
using GuestCounter.Errors;
using GuestCounter.Commands.Counter;

namespace GuestCounter.Controllers {
	public class CounterController : ApiController {
		private dbGuestCounterEntities dbGuestCounter;

		public CounterController() { dbGuestCounter = new dbGuestCounterEntities(); }

		[ActionName("addParticipants")]
		public Model<Object> POST([FromBody] AddParticipantsCMD cmd) {
			if (cmd.DayId != null) {
				giornata grn = (from g in dbGuestCounter.giornata
									 where g.grn_ID == cmd.DayId
									 select g).FirstOrDefault();

				if (grn == null) {
					return (new Model<Object>(CounterErrors.InexistentDay));
				} else {
					if (grn.grn_marcato == true) {
						grn.grn_numeroIngressi = grn.grn_numeroIngressi + 1;
						dbGuestCounter.SaveChanges();
						return (new Model<object>());
					} else {
						return (new Model<Object>(CounterErrors.CancelledDay));
					}
				}
			} else {
				return (new Model<Object>(CounterErrors.NullId));
			}
		}

		[ActionName("saveDay")]
		public Model<Object> POST([FromBody] SaveDayCMD cmd) {
			if (cmd.DayId != null) {
				giornata grn = (from g in dbGuestCounter.giornata
									 where g.grn_ID == cmd.DayId
									 select g).FirstOrDefault();

				if (grn == null) {
					return (new Model<Object>(CounterErrors.InexistentDay));
				} else {
					grn.grn_contato = true;
					grn.grn_marcato = false;
					dbGuestCounter.SaveChanges();
					return (new Model<object>());
				}
			} else {
				return (new Model<Object>(CounterErrors.NullId));
			}
		}

		[ActionName("reset")]
		public Model<Object> POST([FromBody] ResetCMD cmd) {
			if (cmd.DayId != null) {
				giornata grn = (from g in dbGuestCounter.giornata
									 where g.grn_ID == cmd.DayId
									 select g).FirstOrDefault();

				if (grn == null) {
					return (new Model<Object>(CounterErrors.InexistentDay));
				} else {
					grn.grn_contato = false;
					grn.grn_marcato = false;
					grn.grn_numeroIngressi = 0;
					dbGuestCounter.SaveChanges();
					return (new Model<object>());
				}
			} else {
				return (new Model<Object>(CounterErrors.NullId));
			}
		}

		protected override void Dispose(bool disposing) {
			base.Dispose(disposing);
			dbGuestCounter.Dispose();
		}
	}
}

