﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuestCounter.Commands.Event {

	public class AddEventCMD {
		public String Title { get; set; }
		public Int32? MaximumParticipantsNumber { get; set; }
		public Int32? TiketPrice { get; set; }
		public Int32? NumberOfDays { get; set; }
		public String Place { get; set; }
		public String Description { get; set; }
	}

}