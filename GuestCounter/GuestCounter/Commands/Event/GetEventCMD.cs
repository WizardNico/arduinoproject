﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuestCounter.Commands.Event {
	public class GetEventCMD {
		public String Title { get; set; }
		public Int32? EventId { get; set; }
	}
}