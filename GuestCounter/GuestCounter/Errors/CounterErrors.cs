﻿using GuestCounter.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuestCounter.Errors {
	public class CounterErrors {
		public static ControllerError InexistentDay { get { return (new ControllerError(9, "This event's day does not exist")); } }
		public static ControllerError NullId { get { return (new ControllerError(10, "The provided ID is NULL")); } }
		public static ControllerError CancelledDay { get { return (new ControllerError(11, "This day has been cancelled")); } }
	}
}