﻿using GuestCounter.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuestCounter.Errors {

	public class EventErrors {
		public static ControllerError InvalidEventID { get { return (new ControllerError(1, "Invalid Event ID.")); } }
		public static ControllerError InvalidParticipantsNumber { get { return (new ControllerError(2, "Invalid Participants Number.")); } }
		public static ControllerError InvalidTitle { get { return (new ControllerError(3, "Invalid Title For This Event.")); } }
		public static ControllerError InvalidNumberOfDays { get { return (new ControllerError(4, "You Have To Set A Right Number Of Days.")); } }
		public static ControllerError InvalidNumberOfEvents { get { return (new ControllerError(5, "You Have To Create At Least One Event.")); } }
		public static ControllerError DuplicateEventTitle { get { return (new ControllerError(6, "This Title Is Already Used For An Other Event")); } }
		public static ControllerError NoMarkedDay { get { return (new ControllerError(7, "There Is No Marked Day")); } }
		public static ControllerError NotEnoughEvents { get { return (new ControllerError(8, "There Is No Enough Events To Create A Graphic")); } }
	}

}