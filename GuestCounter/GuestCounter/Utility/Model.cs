﻿using System;

namespace GuestCounter.Utility {
	public class Model<T> {
		public Boolean? Success { get; set; }
		public String ErrorDescription { get; set; }
		public Int32? ErrorCode { get; set; }
		public T Data { get; set; }

		public Model() {
			Success = true;
			ErrorDescription = null;
			ErrorCode = null;
			Data = default(T);
		}

		public Model(T data) {
			Success = true;
			ErrorDescription = null;
			ErrorCode = null;
			Data = data;
		}

		public Model(ControllerError error) {
			Success = false;
			ErrorDescription = error.ErrorDescription;
			ErrorCode = error.ErrorCode;
			Data = default(T);
		}
	}
}