﻿using System;

namespace GuestCounter.Utility {
	public class DataManagement {
		public static int age(DateTime birthDate) {
			DateTime today = DateTime.Today;
			int result = today.Year - birthDate.Year;
			if (birthDate > today.AddYears(-result))
				result--;

			return (result);
		}

		public static bool isEmailAddress(string text) {
			if (occurrences(text, "@") == 1 && text.Contains(".") && text.Length >= 5)
				return (true);
			else
				return (false);
		}

		public static bool isPhoneNumber(string text) {
			//consento una occorrenza del carattere '+'
			if (occurrences(text, "+") > 1)
				return (false);

			//controllo se la stringa è convertibile in un numero intero
			long number;
			bool result = Int64.TryParse(text.Replace("+", ""), out number);

			return (result);
		}

		private static int occurrences(string text, string pattern) { return ((text.Length - text.Replace(pattern, "").Length)); }
	}
}